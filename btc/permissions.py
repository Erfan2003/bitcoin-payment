import hashlib
from rest_framework.permissions import BasePermission
from django.conf import settings


class APIKeyPermission(BasePermission):
    def has_permission(self, request, view):
        try:
            api_key = request.headers['Authorization']
        except KeyError:
            return False
        else:
            return hashlib.sha512(api_key.encode()).hexdigest() == settings.API_KEY_HASH
