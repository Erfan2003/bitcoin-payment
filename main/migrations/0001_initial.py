# Generated by Django 3.1 on 2020-08-25 07:17

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('address', models.CharField(max_length=512, unique=True)),
                ('received', models.FloatField(default=0.0)),
            ],
        ),
    ]
