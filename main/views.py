from bitcoinrpc.authproxy import JSONRPCException
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import User
from .helpers import (
    setup_rpc_connection,
    calculate_balance,
    get_address_by_user_id,
    send_from_address,
    list_transactions,
    get_other_address_in_tx
)


@api_view(['GET'])
def create_address_view(request, wallet, user_id):
    rpc_connection = setup_rpc_connection(wallet)
    try:
        User.objects.get(user_id=user_id, wallet=wallet)
    except User.DoesNotExist:
        address = rpc_connection.getnewaddress()
        User.objects.create(user_id=user_id, address=address, wallet=wallet)
        return Response({"Address": address})
    else:
        return Response({'Error': 'Integrity Error: not a unique user id'})


@api_view(['GET'])
def get_balance_view(request, wallet, user_id):
    try:
        address = get_address_by_user_id(user_id, wallet)
    except User.DoesNotExist:
        return Response({'Error': 'User id does not exist'})
    rpc_connection = setup_rpc_connection(wallet)
    balance = calculate_balance(rpc_connection, address)
    return Response({'Balance': '%.08f' % balance})


@api_view(['GET'])
def send_to_user_view(request, wallet, user_id, to_address, amount):
    try:
        from_address = User.objects.get(user_id=user_id, wallet=wallet).address
    except User.DoesNotExist:
        return Response({'Error': 'User id does not exist'})
    amount = float(amount)
    rpc_connection = setup_rpc_connection(wallet)
    try:
        txid = send_from_address(rpc_connection, from_address, to_address, amount)
    except JSONRPCException:
        return Response({'Error': 'Maybe needing more than balance'})
    return Response({'txid': txid})


@api_view(['GET'])
def get_transactions_view(request, wallet, user_id, page):
    try:
        address = get_address_by_user_id(user_id, wallet)
    except User.DoesNotExist:
        return Response({'Error': 'User id does not exist'})
    else:
        rpc_connection = setup_rpc_connection(wallet)
        transactions = list_transactions(rpc_connection, address, page)
        transactions = get_other_address_in_tx(transactions)
        transactions = sorted(transactions, key=lambda x: x['time'])
        return Response(transactions)
