from django.db import models


class User(models.Model):
    user_id = models.CharField(max_length=220)
    address = models.CharField(max_length=1024)
    wallet = models.CharField(max_length=1024)


    class Meta:
        unique_together = [['user_id', 'wallet']]
