from django.urls import path
from .views import (
    create_address_view,
    get_balance_view,
    send_to_user_view,
    get_transactions_view,
)


urlpatterns = [
    path('<str:wallet>/gen_address/<str:user_id>', create_address_view),
    path('<str:wallet>/getbalance/<str:user_id>', get_balance_view),
    path('<str:wallet>/send/<str:user_id>/<str:to_address>/<str:amount>', send_to_user_view),
    path('<str:wallet>/transactions/<str:user_id>/<int:page>/', get_transactions_view),
]
