import json
import requests
from bitcoinrpc.authproxy import AuthServiceProxy
from django.conf import settings
from .models import User


def setup_rpc_connection(wallet):
    rpc_connection = AuthServiceProxy(
        f'http://{settings.RPC_USER}:{settings.RPC_PASSWORD}@127.0.0.1:8332/wallet/{wallet}'
    )
    return rpc_connection


def calculate_balance(rpc_connection: AuthServiceProxy, address: str):
    unspent_list = rpc_connection.listunspent(1, 9999999, [address])
    balance = sum([element['amount'] for element in unspent_list])
    return balance


def send_from_address(rpc_connection: AuthServiceProxy, from_address: str, to_address: str, amount: float):
    unspent_list = rpc_connection.listunspent(1, 9999999, [from_address])
    raw_transaction_input = list(map(
        lambda element: {'txid': element['txid'], 'vout': element['vout']},
        unspent_list,
    ))
    raw_hex = rpc_connection.createrawtransaction(raw_transaction_input, [{to_address: amount}])
    signed_hex = rpc_connection.signrawtransactionwithwallet(raw_hex)
    txid = rpc_connection.sendrawtransaction(signed_hex['hex'])
    return txid


def list_transactions(rpc_connection: AuthServiceProxy, address: str, page: int):
    transactions = rpc_connection.listtransactions("*", 1000*page)
    transactions = filter(lambda element: element['address'] == address, transactions)
    return transactions


def get_other_address_in_tx(transactions):
    txs = []
    for transaction in transactions:
        res = requests.get(f'https://sochain.com/api/v2/tx/BTC/{transaction["txid"]}')
        res = json.loads(res.text)
        input_addr = res['data']['inputs'][0]['address']
        output_addr = res['data']['outputs'][0]['address']
        del transaction['address']
        txs.append({'input_address': input_addr, 'output_address': output_addr, **transaction})
    return txs


def get_address_by_user_id(user_id, wallet):
    return User.objects.get(user_id=user_id, wallet=wallet).address
